### Information about datasets contained
#### cdc ebola
 - website adresses
    - http://www.cdc.gov/vhf/ebola/outbreaks/history/chronology.html
    - http://www.cdc.gov/vhf/ebola/outbreaks/2014-west-africa/case-counts.html  
 - copys of original webpages from which data was extracted can be found in subdirectory

#### em_dat_nat_disasters.csv
 - contains natural disasters obtained from EM-DAT international disaster database using advanced
 search
 - http://www.emdat.be/advanced_search/index.html

#### World Bank data
 - obtained from http://data.worldbank.org/
 - CO2 and GDI data sets 1960 - 2014
