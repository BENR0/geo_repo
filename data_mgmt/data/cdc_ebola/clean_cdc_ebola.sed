#remove all asterisk
s/*//g
#remove pecentages in parenthesis
s/(\([0-9][0-9]%\))//g
s/(\([0-9][0-9].[0-9]%\))//g
s/(\([0-9][0-9][0-9]%\))//g
#remove (asymptomatic)
s/\(asymptomatic\)//
#remove strings like 'November-Dezember'
s/\([A-Za-z]*[-A-Za-z]*\)//
#remove strings from beginning of line
s/[A-Za-z]*//
#change first comma to two commas
s/,/,,/
#remove everything from - to ,,
s/-.*,,/,/
#change ,, back to ,
s/,,/,/
#remove ()
s/()//
#remove remaining (january
s/(January//

