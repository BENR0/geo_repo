D E D U C  KNOWLEDGE PROCESSING SYSTEM

(C) Copyright 1989 by Eduard Pestel Institute for Systems Research (ISP), Hannover (F.R.Germany) and Environmental Systems Research Group, University of Kassel (F.R.Germany)

Brief User Introduction 

Full documentation of the DEDUC system and its application to knowledge processing is found in two German books: 
K.F. Mueller-Reissmann: Wissensverarbeitung mit DEDUC - Handbuch fuer den Benutzer. Vieweg Verlag, Braunschweig/Wiesbaden (Germany) 1989
H. Bossel, B. Hornung, K.F. Mueller-Reissmann: Wissensdynamik - Wirkungsanalyse, Folgenabschaetzung und Folgenbewertung mit DEDUC-Wissensverarbeitung. Vieweg Verlag, Braunschweig/Wiesbaden (Germany) 1989.

The DEDUC system and several sample knowledge bases are provided on a computer disk (IBM-PC format) which comes with this set of books.

DEDUC can be used for
(1) Processing factual knowledge in the sense of deriving conclusions from existing knowledge: These conclusions are used to derive further conclusions, until all possible implications have been derived (impact analysis). Factual knowledge is stored in the  'knowledge module'.
(2) Assessing impacts with respect to value criteria (orientors): Knowledge about orientors and their relationships to potential impact variables is stored in the 'orientor module'. The results of the impact analysis can be transferred into the corresponding orientor module in order to assess what the impacts mean for system development (impact assessment). 

In the example below we document both a simple knowledge module and corresponding orientor module, and demonstrate the use of DEDUC by working with these modules.

Knowledge bases should be written using a text editor (unformatted). They must then be copied into one of nine special files DEDF.1 ... DEDF.9, from which they can be read into DEDUC by using the corresponding commands RF 1 ... RF 9 (see below). The knowledge bases may contain comments enclosed in '!'.

The following program parts are required for running DEDUC:
DEDUC.EXE, DEDUCF.DAT, DEDUCF.TEX.
The program requires about 480 kB of free memory to operate properly. A sufficient number of files and buffers must be defined in CONFIG.SYS:
     FILES = 20
     BUFFERS = 20


!DEDUC-DEMONSTRATION I - KNOWLEDGE MODULE - DEMKNOM!

!Object Structure Definitions!

rawMaterial (coal, oil, iron), fertilizer, wheat is resource.
indProduct (fertilizer), agricProduct (wheat), bread is product.
indCountry (Germany, USA), developgCountry (India) is country.
present, nearFuture, farFuture is tim.

!Implications!

if   (scarce (resource, country, tim)
or   expensive (resource, country, tim))
and  required (resource, product, country, tim)
then expensive (product, country, tim).

!Premises!

prem scarce (oil, country, nearFuture),
     required (oil, fertilizer, country, nearFuture),
     required (fertilizer, agricProduct, indCountry, tim),
     required (wheat, bread, Germany, tim).


!DEDUC DEMONSTRATION II - ORIENTOR MODULE - DEMORIM!

!Object structure!

physExistence, security, freedom, efficiency, adaptivity
is basicOrientor.
health, nutrition, socialSecurity is orientor.
bread, potato is basicFood.

!add Object Structure Definitions of knowledge module:!

rawMaterial (coal, oil, iron), fertilizer, wheat is resource.
indProduct (fertilizer), agricProduct (wheat), bread is product.
indCountry (Germany, USA), developgCountry (India) is country.
present, nearFuture, farFuture is tim.

!Implications!

if   expensive (basicFood, country, tim)
then threat (nutrition, country, tim).

if   threat (orientor, country, tim)
and  important (orientor, basicOrientor, country, tim)
then threat (basicOrientor, country, tim).

!Premises!

prem important (nutrition, physExistence, country, tim),
     expensive (bread, Germany, nearFuture).


In the following example, the knowledge module DEMKNOM is copied into DEDF.2, while the orientor module DEMORIM is copied into DEDF.3. First, the orientor module is read in by using RF 3. This module is saved, and the knowledge module is subsequently read in by using RF 2. After defining the time chain using the TIME command, conclusions corresponding to the given premises are drawn in the knowledge module (using CONCL). These conclusions are then transferred into the orientor module. After defining the time chain there, conclusions are drawn giving the assessment of the impacts on the value set of the orientor module.

In applications not requiring orientor assessment, create the knowledge module by reading in the corresponding knowledge base, define the time chain (if relevant), and draw conclusions using the CONCL command. 

The program is started by typing DEDUC. All commands (except NEXT, BACK, FIN, NOMOTO) must end with a full stop '.'. The available DEDUC-commands are listed and explained below. You can produce these explanations on the screen by typing 'HELP.'.


DEDUC application record:

C:\DEDUX>deduc
D E D U C
KNOWLEDGE PROCESSING SYSTEM
Version 1-1989

NAME OF THE MODULE ?
demorim
NEW MODULE
KNOWLEDGE MODULE ? (Y/N)
n
TYPE YOUR COMMANDS
recon.
 RECORD ON ***********                                             
 OK:                                                               
rf 3.                                                              
 DO YOU WANT TO SEE THE CONCEPTS READ ? (Y/N/BACK)                 
n                                                                  
 OK:                                                               
 END OF RF. OK:                                                    
end.                                                               
 SAVE CURRENT MODULE ? (Y/N/BACK)                                  
y                                                                  
 MODULES:  1 - FREE RECORDS: 19826
 DELETE ANY MODULE ? (Y/N/BACK)                                    
n                                                                  
 CONTINUE WITH ANOTHER MODULE ? (Y/N)                              
y                                                                  
 STORED MODULES ARE:                                               
 demorim         
 NAME OF THE MODULE ?                                              
demknom                                                            
 NEW MODULE                                                        
 KNOWLEDGE MODULE ? (Y/N)                                          
y                                                                  
 DEDUCTION IN MODUS PONENS ONLY ? (Y/N)                            
y                                                                  
 TYPE YOUR COMMANDS                                                
 OK:                                                               
rf 2.                                                              
 DO YOU WANT TO SEE THE CONCEPTS READ ? (Y/N/BACK)                 
n                                                                  
 OK:                                                               
 END OF RF. OK:                                                    
time.                                                              
 SPECIFY THE TIME CHAIN                                            
 TIME  1 ?
present                                                            
 TIME  2 ?
nearFuture                                                         
 TIME  3 ?
farFuture                                                          
 TIME  4 ?
fin                                                                
 OK:                                                               
concl.                                                             
 CONCLUSION INTERRUPT AFTER HOW MANY MINUTES ?                     
1                                                                  
 CUTOFF AT WHICH CERTAINTY FACTOR ?                                
0                                                                  
    1    5) CF:100  expensive(fertilizer,country,nearFuture)
    1    6) CF:100  expensive(agricProduct,indCountry,nearFuture)
    1    7) CF:100  expensive(bread,Germany,nearFuture)
 DELETE THE CONCLUSIONS (1) ? (Y/N)
n                                                                  
 PREMISES AND CONCLUSIONS STORED. YOU MAY ADD SOME PREMISES        
 IMPL.:   1     PREM.:  4     CONCL.:   3
*** CONCL.TIME   12.69 SECONDS

 OK:                                                               
end.                                                               
 SAVE CURRENT MODULE ? (Y/N/BACK)                                  
y                                                                  
 TRANSFER PREMISES/CONCLUSIONS TO ANOTHER MODULE ? (Y/N/BACK)      
y                                                                  
 STORED MODULES ARE:                                               
 demorim          demknom         
 ENTER TARGET MODULE (OR BACK)                                     
demorim                                                            
 ORIENTOR MODULE                                                   
 DEFINE PAIRS OF PREFIXES ? (Y/N)                                  
n                                                                  
 SPECIFY THE TIME CHAIN                                            
 TIME  1 ?
present                                                            
 TIME  2 ?
nearFuture                                                         
 TIME  3 ?
farFuture                                                          
 TIME  4 ?
fin                                                                
 TYPE YOUR COMMANDS                                                
 OK:                                                               
concl.                                                             
 CONCLUSION INTERRUPT AFTER HOW MANY MINUTES ?                     
1                                                                  
 CUTOFF AT WHICH CERTAINTY FACTOR ?                                
0                                                                  
 SATURATION LIMIT FOR EVALUATION ? (Y/N)                           
n                                                                  
    7) CF:100  AF: 100  threat(nutrition,Germany,nearFuture)
    8) CF:100  AF: 100  threat(physExistence,Germany,nearFuture)
 DELETE THE CONCLUSIONS (1) ? (Y/N)
n                                                                  
 PREMISES AND CONCLUSIONS STORED. YOU MAY ADD SOME PREMISES        
 IMPL.:   2     PREM.:  6     CONCL.:   2
*** CONCL.TIME   61.03 SECONDS
 OK:                                                               
end.
SAVE CURRENT MODULE ? (Y/N/BACK)
n
TRANSFER PREMISES/CONCLUSIONS TO ANOTHER MODULE ? (Y/N/BACK)
n
MODULES: 2 - FREE RECORDS: 19729
DELETE ANY MODULE ? (Y/N/BACK)
n
CONTINUE WITH ANOTHER MODULE ? (Y/N)
n
--- PROGRAM FINISHED
****START TIME 10: 6:47
****END   TIME 10:29:50
Stop - Program terminated. 


LIST OF DEDUC COMMANDS (produced by typing HELP.)
  
 COMMAND SUMMARY:                                                  
 1  READING / WRITING TO FILES                                     
 2  INPUT OF CONCEPTS                                              
 3  INFORMATION ABOUT CONCEPTS                                     
 4  MODIFICATION OF CONCEPTS                                       
 5  PROCESSING COMMANDS                                            
 6  EVALUATION COMMANDS (ORIENTOR MODULE ONLY)                     
 7  DIALOGUE CONTROL COMMANDS                                      
 -----------------------------------------------------------------
1                                                                  
 *** READING / WRITING COMMANDS:                                   
 RF   <FILENR>.         Read  File  FILENR                         
 RCF  <FILENR,NR>.      Read Certainty Factors for implications    
                        from FILENR, column NR                     
 RCFP <FILENR,NR>.      Read Certainty Factors for Premises        
                        from FILENR, column NR                     
 RLF  <FILENR,NR).      Read Load Factors for implications         
                        from FILENR, column NR                     
 RAF  <FILENR,NR).      Read Affectedness Factors for premises     
                        from FILENR, column NR                     
    FILENR: 1 ...  9  (DEDF.1 ... DEDF.9)                          
    NR:     1 ... 12                                               
 RECON.                 RECord ON                                  
 RECOFF.                RECord OFF                                 
      (record on file DEDF.10)                                     
------------------------------------------------------------------
2                                                                  
 *** INPUT OF CONCEPTS:                                            
 <OBJECT STRUCTURE> IS <OBJECT>.                                   
      definition of object structure                               
 IF <PREDICATE EXPRESSION> THEN <PREDICATE LIST> [<CF>].           
      definition of implication                                    
      CF  Certainty Factor: 0 ... 100                              
 LF  <LOAD FACTOR>.                                                
      definition of Load Factor for preceding implication          
      LOAD FACTOR: -100 ... 100  (only in orientor module)         
 PREM <PREMISE LIST> [<CF>].                                       
      definition of PREMises                                       
 AF   <AFFECTEDNESS FACTOR>.                                       
      definition of Affectedness Factor for preceding premise      
      AFFECTEDNESS FACTOR: -100 ... 100  (only in orientor module) 
 VALUE <NR>.     definition of VALUE set NR for evaluation         
 SEV   <NR>.     SElection of Value set NR. for evaluation         
      NR: 1 ... 5                                                  
 TIME.           definition of TIME chain                          
 -----------------------------------------------------------------
3                                                                  
 *** INFORMATION ABOUT CONCEPTS:                                   
 1  SORT COMMANDS                                                  
 2  PRINT OBJECTS                                                  
 3  PRINT IMPLICATIONS                                             
 4  PRINT PREMISES/CONCLUSIONS                                     
 5  SELECTED OUTPUT                                                
 6  PRINT VALUES                                                   
 7  PRINT TIME CHAIN                                               
 8  STATUS                                                         
 -----------------------------------------------------------------
1                                                                  
 *** SORT COMMANDS:                                                
 SORT OBJ.              SORT OBJects                               
 SORT PRED.             SORT PREDicates                            
 PRIA.                  PRInt Atomic expressions                   
 PRIASO <OBJECT LIST>.  PRInt Atomic expressions containing        
                        all objects of OBJECT LIST                 
 -----------------------------------------------------------------
2                                                                  
 *** PRINT OBJECTS:                                                
 PRIO ALL.              PRInt ALL top Objects                      
 PRIO <OBJECT>.         PRInt Object structure with top object OBJECT          
 -----------------------------------------------------------------
3                                                                  
 *** PRINT IMPLICATIONS:                                           
 PRI ALL.               PRint ALL Implications                     
 PRI <PREDICATE>.       PRint Implications containing PREDICATE    
                        on left hand side                          
 PRI <NR>.              PRint Implication NR                       
 PRI <NR>,.             PRint Implications, starting with implication NR       
 PRI <NR1>,<NR2>.       PRint Implications NR1 through NR2         
 PRIR <PREDICATE>.      PRint Implications containing PREDICATE    
                        on Right hand side                         
 PRISO <OBJECT LIST>.   PRint Implications Selected according      
                        to Objects on OBJECT LIST                  
 -----------------------------------------------------------------
4                                                                  
 *** PRINT PREMISES/CONCLUSIONS:                                   
 PRIP ALL.              PRInt ALL Premises/conclusions             
 PRIP <PREDICATE>.      PRInt Premises/conclusions containing PREDICATE        
 PRIP <NR>.             PRInt Premise/conclusion NR                
 PRIP <NR>,.            PRInt Premises/conclusions, starting with NR           
 PRIP <NR1>,<NR2>.      PRInt Premises/conclusions NR1 through NR2 
 PRIPSO <OBJECT LIST>.  PRInt Premises/conclusions Selected according          
                        to Objects on OBJECT LIST                  
 PRIP.                  PRInt all Premises (of preceding conclusion process)   
 PRIC.                  PRInt all Conclusions (of preceding conclusion process)
 PRIPCF.                PRInt Premises/conclusions in order of     
                        decreasing Certainty Factor                
 PRIPCF <CF1>,<CF2>.    PRInt Premises/conclusions with Certainty Factor       
                        between CF1 and CF2                        
 -----------------------------------------------------------------
5                                                                  
 *** SELECTED OUTPUT:                                              
 SE.          start SElected output (after PRI <PREDICATE>, PRIR or PRISO      
              resp. PRIP <PREDICATE> or PRIPSO)                    
 ES.          End Selected output                                  
 -----------------------------------------------------------------
6                                                                  
 *** PRINT VALUES:                                                 
 PRIV  <NR>.     PRInt Value set NR                                
 -----------------------------------------------------------------
7                                                                  
 *** PRINT TIME CHAIN:                                             
 PRIT.           PRInt Time chain                                  
 -----------------------------------------------------------------
8                                                                  
 *** STATUS:                                                       
 STAT.         report STATus of available memory space, module type etc.       
 -----------------------------------------------------------------
4                                                                  
 *** MODIFICATION OF CONCEPTS:                                     
 1  DEFINE SYNONYMOUS PREDICATES                                   
 2  DELETE OBJECTS                                                 
 3  DELETE IMPLICATIONS                                            
 4  DELETE PREMISES/CONCLUSIONS                                    
 5  CHANGE CERTAINTY / LOAD / AFFECTEDNESS FACTORS                 
 6  CHANCE VALUE SET                                               
 7  CERTAINTY FACTOR INTERVALS                                     
 -----------------------------------------------------------------
1                                                                  
 *** DEFINE SYNONYMOUS PREDICATES:                                 
 SYN <PREDICATE.NEW>,<PREDICATE.OLD>   define PREDICATE.NEW as SYNonym         
                                       of PREDICATE.OLD            
 -----------------------------------------------------------------
2                                                                  
 *** DELETE OBJECTS:                                               
 DELO ALL.         DELete ALL Objects                              
 DELO <OBJECT>.    DELete Object structure OBJECT                  
 -----------------------------------------------------------------
3                                                                  
 *** DELETE IMPLICATIONS:                                          
 DEL ALL.          DELete ALL implications                         
 DEL <PREDICATE>.  DELete implications containing PREDICATE        
                   on left hand side                               
 DEL <NR>.         DELete implication NR                           
 DEL <NR>,.        DELete implications, starting with implication NR           
 DEL <NR1>,<NR2>.  DELete implications NR1 through NR2             
 -----------------------------------------------------------------
4                                                                  
 *** DELETE PREMISES/CONCLUSIONS:                                  
 DELP ALL.          DELete ALL Premises/conclusions                
 DELP <PREDICATE>.  DELete Premises/conclusions containing PREDICATE           
 DELP <NR>.         DELete Premise/conclusion NR                   
 DELP <NR>,.        DELete Premises/conclusions, starting with NR  
 DELP <NR1>,<NR2>.  DELete Premises/conclusions NR1 through NR2    
 -----------------------------------------------------------------
5                                                                  
 *** CHANGE CERTAINTY / LOAD / AFFECTEDNESS FACTORS:               
 CCF  ALL.       Change Certainty Factor of ALL implications       
 CCF  <NR>.      Change Certainty Factor of implication NR         
 CCFP ALL.       Change Certainty Factor of ALL Premises/conclusions           
 CCFP <NR>.      Change Certainty Factor of Premise/conclusion NR  
 CLF  ALL.       Change Load factor of ALL implications (in orientor module)   
 CLF  <NR>.      Change Load Factor of implication NR (in orientor module)     
 CAF  ALL.       Change Affectedness Factor of ALL premises/conclusions        
                 (in orientor module)                              
 CAF  <NR>.      Change Affectedness Factor of Premise/conclusion NR           
                 (in orientor module)                              
 -----------------------------------------------------------------
6                                                                  
 *** CHANGE VALUE SET:                                             
 CV    <NR>.     Change Value set NR                               
 -----------------------------------------------------------------
7                                                                  
 *** CERTAINTY FACTOR INTERVAL:                                    
 CFINT.      define Certainty Factor INTervals for evaluation      
             (before starting conclusion process in orientor module)           
 CFINTC.     Certainty Factor INTervals Cancellation               
 -----------------------------------------------------------------
5                                                                  
 *** PROCESSING COMMANDS:                                          
 CONCL.                execute CONCLusion process                  
 CONCL <OBJECT LIST>.  execute CONCLusion process, restricting output          
                       to conclusions containing objects on OBJECT LIST        
 HOW <NR>.             explain HOW conclusion NR was generated     
 IMP.                  list IMPlications used in the conclusion process        
 NIMP.                 list IMPlications Not used in the concl. process        
 STOC.                 STOre old Conclusion sets (premises and conclusions)    
 COC 1.                COmpare Conclusion sets; shows identical and different  
                       premises and conclusions                    
 COC 2.                COmpare Conclusion sets; show identical predicates      
 REC.                  RECall old Conclusion set                   
 EVAL.                 generate EVALuation diagram:                
 EVAL A.   affectedness (of values of the selected value set)      
 EVAL B.   weighted affectedness                                   
 EVAL C.   weighted affectedness; certainty factors considered     
 EVAL D.   affectedness; certainty factors considered              
 EVAL E.   aggregated evaluation; separate negative and positive contributions 
 EVAL F.   aggregated evaluation; net sum of neg./pos. contributions           
 EVAL G.   aggregated evaluation with (weighted) summation over time           
 -----------------------------------------------------------------
6                                                                  
 *** EVALUATION COMMANDS:                                          
 LF  <LOAD FACTOR>.                                                
      definition of Load Factor for preceding implication          
      LOAD FACTOR: -100 ... 100  (only in orientor module)         
 AF   <AFFECTEDNESS FACTOR>.                                       
      definition of Affectedness Factor for preceding premise      
      AFFECTEDNESS FACTOR: -100 ... 100  (only in orientor module) 
 RLF  <FILENR,NR).      Read Load Factors for implications         
                        from FILENR, column NR                     
 RAF  <FILENR,NR).      Read Affectedness Factors for premises     
                        from FILENR, column NR                     
    FILENR: 1 ...  9  (DEDF.1 ... DEDF.9)                          
    NR:     1 ... 12                                               
 VALUE <NR>.     definition of VALUE set NR for evaluation         
 PRIV  <NR>.     PRInt Value set NR                                
 CV    <NR>.     Change Value set NR                               
 SEV   <NR>.     SElection of Value set NR. for evaluation         
      NR: 1 ... 5                                                  
 CFINT.      define Certainty Factor INTervals for evaluation      
             (before starting conclusion process in orientor module)           
 CFINTC.     Certainty Factor INTervals Cancellation               
 EVAL.       generate EVALuation diagram (s. PROCESSING COMMANDS)  
 OK:                                                               
 -----------------------------------------------------------------
7                                                                  
 * DIALOGUE CONTROL COMMANDS (without point):                      
 BACK or B       go BACK to previous step                          
 FIN or F        FINish this part (inside of commands TIME., VALUE <NR>.,      
                 CCF ALL., CCFP ALL., CLF ALL., CAF ALL., CV <NR>.)            
 NEXT or N <NR>  NEXT changing at NR (in CCF ALL. etc.)            
 PRI or P        PRInt implication (in CCF- and CLF-command)       
 PRIP or P       PRInt Premise/conclusion (in CCFP- and CAF-command)           
 NOMOTO          continue without MOdus TOllens (in CONCL-command) 
------------------------------------------------------------------
