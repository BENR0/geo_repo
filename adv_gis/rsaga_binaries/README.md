### Instructions for geospatial R packages
These packages are compiled for Mac OS X Mavericks with R version 3.1.2, since the official R Cran
repositories do not contain binaries yet.

It is recomended to install the packages in the following order:
 - xts
 - spacetime
 - gstat
 - RSAGA
 - rgdal
 - rgeos

### Information for usage of RSAGA with QGIS in R
To use the RSAGA package with saga_cmd and saga libraries shipped with QGIS the following RSAGA
enviroment can be defined in R.

	saga_env <- rsaga.env(check.libpath=FALSE,
                 check.SAGA=FALSE,
                 workspace="<DEFINE/YOUR/WORKING/DIRECTORY/HERE>",
                 os.default.path="/Applications/QGIS.app/Contents/MacOS/bin",
                 modules="/Applications/QGIS.app/Contents/MacOS/lib/saga")

It is important to know that the workspace you define in the RSAGA enviroment is not (must not)
necessarily be the same directory as the working directory set by setwd(). The RSAGA enviroment will
only be used for output of RSAGA operations.


### Further information
In the case that instead of the conveniant RSAGA package, system calls are used to interact with the
SAGA tools from the Rstudio application, the system call will most likely quit with an error. This
behaviour, which is circumvented with the RSAGA package by defining the enviroment (see above), is
a result of Rstudio not reading the enviroment variables of the system shell even though the path to
the saga_cmd command and the saga libraries might appropiately be exported.
The solution to this is to open Rstudio from the shell (i.e. Terminal application or the highly
recommended Terminal alternative iTerm) with the command:

    open -a Rstudio

In the case that the path to SAGA has not be set in the shell profile this can be done by adding 

     /Applications/QGIS.app/Contents/MacOS/bin

to the line containing "export PATH=" in the dot file of the appropiate shell (in the case of bash,
which is standard on Mac OS X this file is probably located at ~/.bash_profile). If no .bash_profile file
exists you can create this file and add:

    export $PATH=$PATH:/Applications/QGIS.app/Contents/MacOS/bin

It is important though to check beforehand if a file .profile exists because this file will not be
read anymore if .bash_profile is created.

Furtermore the path to the SAGA libraries needs to be exported as follows:

    export SAGA_MLB=/Applications/QGIS.app/Contents/MacOS/lib/saga
