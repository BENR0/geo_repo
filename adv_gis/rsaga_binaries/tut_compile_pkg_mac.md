### Abstract
When working with R on Mac OS X sometimes needed binary packages are not available in the latest
version. This is a problem when trying to install a package which is thus incompatible with the used
R version.

The Solution to this is to compile the needed package yourselfe.

### What you need
  - installed R
  - xcode developer tools
  - depending on the package additional compilers like gfortran

### How to get the xcode developer tools
Since we have the problem named above it is assumed that R is already installed on the system. Thus
this leaves the last to points of section two.

To get the developer tools open Terminal.app and type the command 'make' (without the tick marks).
This will ask you if you want to install the developer tools.

>##### Explanation for the interested
> The xcode developer tools contain the most common compilers and libraries. Since we want to compile
> packages we thus need compilers. The make command ist a standard comman on Unix/Linux systems to
> compile software. Since this command can only be used if xcode command line tools are installed Mac
> OS asks if they should be installed when issuing this command.

Unfortunately the xcode command line tools contain not all compilers which are sometimes needed.
This holds true for example for gfortran. In consequence if gfortran is required it needs to be
installed separately. gfortran binaries for Mac OS can be obtained
[here]([https://gcc.gnu.org/wiki/GFortranBinaries#MacOS).

### Compiling the Rsaga package
In the following as an example we will compile a packages which is usefull for GIS analyis
in R. Namely this is Rsaga package, for which often there are no binary images of
the latest Version available. The Rsaga package unfortunately has some dependencies which we might
need to compile ourselves to depending on the availability of binaries. This includes the following

  - rgdal
  - rgeos
  - spacetime
  - xts
  - gstat

It is important to know that in order to be able to compile for example the rgeos and the rgdal
package the frameworks of both of these tools need to be installed on the system since the
R packages only provide wrappers for these. In case you already installed QGIS on your system you
can continue because previous to installing QGIS you already installed the named frameworks
(possibly with the binary installers provided by [kyngchaos]([http://www.kyngchaos.com/software/qgis)).
This is probably the most comfortable way since QGIS also contains the SAGA libraries we can use
with the Rsaga package we are compiling.

>##### Alternative fo the interested
> If you don't want to bloat your syste with QGIS and don't need the SAGA libraries, there is
> a different approach to obtain the frameworks without the binary installers (which by the way could still be used).
> The package manager 'Homebrew' provides a conveniant way to install rgdal and many more software
> packages available for Unix/ Linux. Even though this tutorial will not describe this way, 'Homebrew'
> is well documented and can be found [here](http://brew.sh/).

### Compiling
After everything is setup we can now start compiling the R packages, for which we will need the
source code. The source code for each package can be found on the their respective R cran package
website. These can be found by googeling 'R cran <name>', where <name> is the name of the package we
are looking for. For example the R cran site for the rgdal package looks like [this](http://cran.r-project.org/web/packages/rgdal/).
The generall layout for all package sites is the
same and the source code can always be found under 'Package source'. When looking for the source
code a little further down we can see if a binary package for Mac OS is available for our specific
version of the operating system. If it is not we stick to our plan of compiling it ourvelves and go
ahead with downloading the source code. Save the tar.gz archive in a directory of your choice (in
this tutorial we will use ~/Downloads/source/). Repeat this with all packages Rsaga depends on.
Once you are done open Terminal.app and change your directory to where you downloaded the sources.
In our case

    cd Downloads/source

Now in order to compile the packages we use the command

    R CMD INSTALL <name>

where <name> is the name of the package we want to compile.
This will compile and install the package in the to the standard library path of the R installation.
We use the same command to install all desired packages.
In the case of dependencies we need to compile the packages in the reverse order of the dependancie
of the last package we want to install. Fortunately if a dependancie is not met while compiling we
will get an error message telling us about it (general tip for dealing with errors: use the error
message as a test for a google search. Most often somebody else already had a similar or the same
problem and somebody might have had a usefull answer.)
Once you compiled all packages your are done and can close Terminal.app and try to load them in
a R session.

>##### For the interested
> Sometimes you might already have a working installation of a package and don't want to upgrade it to
> a newer version and/ or you might additionally want to compile a binary of the package. In this case
> you need to provide additional flags to the command used above as follows.
> 
>     R CMD INSTALL --build -l  <path/to/desired/installation> <name>
> 
> This uses the --build flag to compile binaries and the -l flag to specify the path to the desired
> installation destination. If a path and the build flag is given the binary will be saved to this
> path also.

### Further notes
In general a help about all options with a short description can be view by typing

    R CMD INSTALL --help

For more explanation of different compiling options see the [R cran
manual](http://cran.r-project.org/doc/manuals/r-release/R-admin.html#Installing-packages)

### Information for usage of RSAGA with QGIS in R
To use the RSAGA package with saga_cmd and saga libraries shipped with QGIS the following RSAGA
enviroment can be defined in R.

	saga_env <- rsaga.env(check.libpath=FALSE,
                 check.SAGA=FALSE,
                 workspace="<DEFINE/YOUR/WORKING/DIRECTORY/HERE>",
                 os.default.path="/Applications/QGIS.app/Contents/MacOS/bin",
                 modules="/Applications/QGIS.app/Contents/MacOS/lib/saga")

It is important to know that the workspace you define in the RSAGA enviroment is not (must not)
necessarily be the same directory as the working directory set by setwd(). The RSAGA enviroment will
only be used for output of RSAGA operations.


### Further information
In the case that instead of the conveniant RSAGA package, system calls are used to interact with the
SAGA tools from the Rstudio application, the system call will most likely quit with an error. This
behaviour, which is circumvented with the RSAGA package by defining the enviroment (see above), is
a result of Rstudio not reading the enviroment variables of the system shell even though the path to
the saga_cmd command and the saga libraries might appropiately be exported.
The solution to this is to open Rstudio from the shell (i.e. Terminal application or the highly
recommended Terminal alternative iTerm) with the command:

    open -a Rstudio

In the case that the path to SAGA has not be set in the shell profile this can be done by adding 

     /Applications/QGIS.app/Contents/MacOS/bin

to the line containing "export PATH=" in the dot file of the appropiate shell (in the case of bash,
which is standard on Mac OS X this file is probably located at ~/.bash_profile). If no .bash_profile file
exists you can create this file and add:

    export $PATH=$PATH:/Applications/QGIS.app/Contents/MacOS/bin

It is important though to check beforehand if a file .profile exists because this file will not be
read anymore if .bash_profile is created.

Furtermore the path to the SAGA libraries needs to be exported as follows:

    export $SAGA_MLB=/Applications/QGIS.app/Contents/MacOS/lib/saga

