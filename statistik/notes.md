### multiple linear regression
  - multicolinearity -> VIF test value needs to be smaller than 5 or 10 respectively (car package)
  - heteroscedacity -> bptest (lmtest package)
  - (temporal) autocorrelation -> durbin.watson (for time data)

### logistic regresseion
  - glm models
  - no test of normal distribution or heteroscedacity
  - VIF yes
  - durbin.watson yes

### spatial regression
  - spatial autocorrelation -> moran.test (spdep package)
  - mat2list() transfers matrix to list
  - cross regressive model -> independant variables have impact beyond regional borders
    - create additional variable newvar <- lag.listw(weights,var)
  - spatial autoregressive model -> dependant variables spread on neighbouring regions (diffusion)
      summary(lagsarlm(var1~var2+...,listw=weights)) rho: parameter for auto regressive term
  - spatial error model summary(errorsarlm(var1~var2+...,listw=weights)) lambda: spatially
      correlated error term parameter


### probe klausur besprechung
 - morans i test räumliche autokorrelation
 - breusch pagan zeitliche autokorrelation!?
 - shapiro (auf residuen anwenden) test auf normalverteilung
 - durbin watson test ?
 - vif test ?
 - odTest ?
 - autokorrelation anwenden auf residuen
 - zwischen negbin und poission regression nicht frei wählen sondern das wählen was besser
     beschreibt. aic vergleichen oder odtest(overdispersion). aufgabe a3 e
 - aufgabe 4 a: tests ok aber zeitliche autokorrelation fehlt
 - für zählgrößen (anzahl an vögeln)  statt lineare regression  poisson oder negativ binomiale
     regression wählen (im fall von vögeln jeden tag gezählt zeitreihenmethode "noch besser") 
 - aufgabe 5 b: lineare reg durchführen und aic vergleichen oder überprüfungstest für lineare reg
     durchführen und schauen ob diese funktionieren. (alternativ beide regressionen machen sowie
     deren tests und beide vergleichen)
 - odtest - reicht poission verteilung aus. poissoin verteilung 1 parameter -> breite festgelegt
     durch mitte. negbin 2 parameter breite unabhänig von lage kann sich besser an overdispersion
     anpassen

 - packete spdep, mass, cars, lm, pscl 
